FROM python:latest

RUN mkdir /etc/src/

WORKDIR /etc/src/

COPY ./src .

RUN pip install -r requirements.txt

RUN chmod u+x run.sh

CMD ["./run.sh"]